---
layout: post
title:  "ReasonML Journey Part II: Subtyping and ExtendableEvent"
date:   2020-06-11
---
The first stop on my quest to wrap a JavaScript library in ReasonML is a small,
simple JavaScript interface
[ExtendableEvent](https://developer.mozilla.org/en-US/docs/Web/API/ExtendableEvent),
a class with no properties and only one method on top of what it inherits from
[Event](https://developer.mozilla.org/en-US/docs/Web/API/Event).
In this brief blog post I will set out a simple template for subtyping in
ReasonML. 

### Source as a learning resource
There are not many BuckleScript tutorials out there, so in addition to the 
official documents, I've found that one of the best ways to learn ReasonML is
by reading the
[ReasonML community libraries](https://github.com/reasonml-community/).
The [bs-webapi-incubator](https://github.com/reasonml-community/bs-webapi-incubator/blob/master/README.md), 
for example, includes a helpful explanation of subtyping and inheritance in 
ReasonML. It gives the following example: 

```ocaml
type _element('a);
type element_like('a) = node_like(_element('a));
type element = element_like(_baseClass);
```

Candidly, while I think I pretty much understand what's going on here, I do
not understand it well enough to explain it, so that will be left as an
exercise for the user or for a future blog post after I get my bearings. For
now, I'm satisfied to know that this code makes a type `element` that's a 
subtype of `node`, or rather, it's close cousin `node_like` which takes a type
parameter.

This code snippet is an example of *source code* from the standard library
itself, not a project that references the standard library, so to make this
code compile, we need to reference the Dom module in the standard library like
so: 

```ocaml
type _element('a);
type element_like('a) = Dom.node_like(_element('a));
type element = element_like(Dom._baseClass);
```

This code snippent will compile, and we're now well on our way to recreating
ReasonML's element type. 

But that's not what we're here for. We're here to write create
`ExtendableEvent` from `Event`, and even though I only kind of understand this 
code, I can pretty copy this format for my use case:

```ocaml
type _extendableEvent('a);
type extendableEvent_like('a) = Dom.event_like(_extendableEvent('a));
type extendableEvent = extendableEvent_like(Dom._baseClass);
```

Now all I have to do is paste this into a *.re* file in *src/* and run `npm 
run build`, and I have the first step done. You'll have to `waitUntil` the next
blog post to see how to extend this subtype to include a new method.
