---
layout: post
title:  "ReasonML Journey Part III: Generics, Promises, and ExtendableEvent.waitUntil()"
date:   2020-06-12
tags: ['bucklescript', 'promise', 'reasonml']
---

This post continues the quest of trying to recreate the JavaScript service 
worker class
[ExtendableEvent](https://developer.mozilla.org/en-US/docs/Web/API/ExtendableEvent) in ReasonML. In this post I will show how to extend the subtype of `event`
to include `ExtendableEvent`'s [waitUntil](https://developer.mozilla.org/en-US/docs/Web/API/ExtendableEvent/waitUntil) method, which takes a generic
Promise as a parameter. 

The [BuckleScript official documentation](https://bucklescript.github.io/docs/en/function)
gives us a good starting point for defining functions that reference JavaScript
object methods, as shown below: 

{% highlight ocaml %}
[@bs.send] external getElementById: (document, string) => Dom.element 
    = "getElementById";
{% endhighlight %}

In this example, `[@bs.send] external` tells BuckleScript to call out 
to JavaScript, 
trusting that there is such a function as the string `"getElementById"`. The 
rest of the statement provides the types we will use to interact with it. We 
need to adapt this to take a type parameter for our promise.

{% highlight ocaml %}
[@bs.send] external waitUntil: (extendableEvent, Js.Promise.t('a)) => unit
    = "waitUntil";
{% endhighlight %}

This gives us access to the JavaScript function `"waitUntil"`, which takes our
extendableEvent as a first "parameter" (in this context) and a strongly typed
promise, returning `unit`, which, in functional languages, is like `void`.

And we're done for the day. In the next post, I'll work on the npm side of 
things so that we can use our new type elsewhere. 
