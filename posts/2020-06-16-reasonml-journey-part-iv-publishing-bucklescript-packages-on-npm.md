---
layout: post
title:  "ReasonML Journey Part IV: Publishing BuckleScript Packages on NPM"
date:   2020-06-16
tags: ['bucklescript', 'npm', 'reasonml']
---

In the previous post, we finished wrapping
[ExtendableEvent](https://developer.mozilla.org/en-US/docs/Web/API/ExtendableEvent) in ReasonML. In this post I will publish our type on npm. 

### bsconfig.json

I will start by making two further changes in the *bsconfig.json*. First, I 
want to set the "version" to "0.0.1" to best reflect the state of the project. 
More importantly, I want to change the "namespace" attribute toward the bottom
of the file. At the time of this post, `npx bsb -init $projectName theme 
basic-reason` produces a default namespace of `true`. This results in a default
namespace to match the "name", so in my case, it would prefix my module 
with the namespace of `BsServiceWorker`.

The resulting *bsconfig.json* follows: 

{% highlight json %}
{% raw %}
{
    "name": "bs-service-worker",
    "version": "0.0.1",
    "sources": {
    "dir" : "src",
    "subdirs" : true,
    "public": "all"
    },
    "package-specs": {
        "module": "commonjs",
        "in-source": true
    },
    "suffix": ".bs.js",
    "bs-dependencies": [
    ],
    "warnings": {
        "error" : "+101"
    },
    "namespace": "ServiceWorker",
    "refmt": 3
}
{% endraw %}
{% endhighlight %}

### Writing a good *README.md*
The official redex documentation has some good 
[recommendations](https://redex.github.io/publish) for what should go into a
*README.md*. For now, I'm going to forgo a changelog in favor of a Todo list.
A changelog will be more useful after I hit a major version. I'm also going to
put off usage examples because I don't have enough written to effectively use
yet. The rest of the suggestions are very applicable and I have incorporated 
them into a good starter *README.md*. 

{% highlight md %}
# bs-service-worker

This package will *eventually* be the home of BuckleScript bindings for the
[JavaScript service worker API](https://developer.mozilla.org/en-US/docs/Web/API/Service_Worker_API). 
It is currently in a very early stage of development.

## Installation
`npm install bs-service-worker`

## Implemented
- [x] [ExtendableEvent](https://developer.mozilla.org/en-US/docs/Web/API/ExtendableEvent)
{% endhighlight %}

### Testing Locally
The official npm documentation 
[recommends](https://docs.npmjs.com/creating-node-js-modules) testing your 
package locally. Translating those steps to the BuckleScript ecosystem
looks like: 

10. **Initialize a new ReasonML project.** For a description of my own setup, 
see [part one of this series](/TODO).
20. **Install your package from local** using the relative path. For example,
I will run `npm install ../bs-service-worker`.
30. **Add your package by name to the bs-dependencies** of your new project. 
For example, I will add "bs-service-worker".
40. **Reference your package using the namespace of your project** in a source
file. For example, in *src/Demo.re*, I will add `open ServiceWorker;`.

If your project builds and your using code runs as expected, you may be ready
to publish using `npm login` and `npm publish`.

When I'm a little further along, I'll also want to publish my npm registery
information to [the redex](https://npmjs.com/package/package-name), but it
would be misleading at this point to advertise my package that only wraps one
type. 

### Thanks for riding along with me!
This concludes my four-part series on setting up a BuckleScript library in 
Reason. I'll still have plenty to write about ReasonML 
going forward as I build out my binding, and as a beginner, I'm glad I don't
have to be worried about not having the ability to publish my work. Turns out,
it's pretty simple. 
