---
layout: post
title:  "ReasonML Journey Part I: Getting Started with BuckleScript"
date:   2020-06-10
tags: ['bucklescript', 'reasonml']
---
ReasonML is, at the time of this post, a very young language and, as such, very
much underdocumented. It's also missing a lot of the JavaScript standard 
libraries. I see both of these things as an opportunity to contribute both
to the foundational documentation and to the libraries that haven't been
written yet. This blog series will serve mainly as a resource to me as an 
absolute beginner trying to retrace my steps, but I hope that someone else
may someday find it useful as well. 

This post in particular will cover the first steps of initiating an npm package
module and a ReasonML project in BuckleScript. The purpose of this post is not
as much instructional as it is contextual, providing a way to recreate the
precise environment in which the code was written.

### Prerequisites
* You know basic terminal commands like `ls` and `cd`.
* npm is installed.
* git is installed, and you know how to create and clone a repository.

### Setting up your BuckleScript workbench
Whenever I can, I avoid installing npm packages globally. I prefer to have each
project maintain its own dependencies that can be updated independently
without affecting one another, so I will be setting up my environment in a 
very specific way. 

10. **In your workspace, create a new directory** for this and all future
BuckleScript projects. For example, I created *~/workbench/bucklescript/*.
20. **Enter that directory in a terminal.**
30. **Run `npm init -y` to initialize your workspace.** The `-y` tells npm to
adopt all defaults in the npm *package.json*, which is what we want because
this is not the project directory. Instead, this is the directory where we're
going to create project directories. 
40. **Run `npm install bs-platform`** to install the BuckleScript platform
locally. 

Now the BuckleScript platform can create new BuckleScript projects in your 
directory. In the next section, we will be creating a new ReasonML project.

### Creating a new project
10. **Choose a name for your project.** I have decided to name my project
*bs-service-worker* because my ultimate goal will be to write a service worker
library for BuckleScript, the development environment for ReasonML and OCAML.
20. **Run `npx bsb -init $projectName -theme basic-reason`** to create your
new project directory.
30. **Enter this new project directory.**
40. **Install the bucklescript platform in this directory**, which is already
an npm package, by running `npm install bs-platform`. (Recall that it was only
installed locally in the parent folder, so we do need to install it again here
to use the BuckleScript platform.)
50. **Open your *package.json*.**
60. **Edit each member of the scripts property by prefixing each command with 
"npx".** For example, "build" correspond to "npx bsb -make-world" instead of
"bsb -make-world. Do this for each command to reflect that we've installed
BuckleScript only locally.
70. **Edit other project metadata in *package.json*** as you see fit. I 
like to set:
* my version to 0.0.1.
* my keywords to accepted Redex tags.
* the author to include my first name and my blog.
* the license to something open.
80. **Close your *package.json*.**
90. Test your new project's setup by **running `npm run build`.**
100. Optionally, initialize a git repository and add a remote *origin*, but be
careful not to automatically create a *README.md* when creating your remote
repository or you will have merge conflicts.

### Conclusion
If your build command ran successfully, you have a new ReasonML project. In
the next section, we will write some code for it. 
