---
layout: layouts/post.njk
title: About
eleventyNavigation:
  key: About Me
  order: 3
---

### Web
I'm an enthusiastic functional programmer and a web generalist. My gateway 
drug was learning Scala at DePaul University. Now I'm learning ReasonML, which
is the current focus of this blog, but will not be the sole focus. 

### Bureaucrat

I'm a proud career civil servant dedicated to modernizing government technology
for the common good. I got swept up in civic tech twitter in college and never
looked back. 

### Contact

Have a question or a correction for me? Feel free to hit me up as
[@webbureaucrat](https://twitter.com/webbureaucrat) on Twitter. 

